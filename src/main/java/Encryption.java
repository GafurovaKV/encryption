import java.util.Scanner;

public class Encryption {
    public static void main(String[] args) {
        System.out.println("Нажмите [ Q ] - для шифрования, [ W ]- для расшифровки");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();

        if (choice.equals("Q")) {
            System.out.println("Введите текст для шифрования");
            Scanner input1 = new Scanner(System.in);
            String text = input1.next();
            System.out.println("Введите ключ от 1 - 9");
            Scanner input2 = new Scanner(System.in);
            int key = input2.nextInt();
            Encoder encoder = new Encoder(text, key);
            encoder.encode(text, key);

        } else if (choice.equals("W")) {
            System.out.println("Введите текст для расшифровки");
            Scanner input1 = new Scanner(System.in);
            String text = input1.next();
            System.out.println("Введите ключ от 1 - 9");
            Scanner input2 = new Scanner(System.in);
            int key = input2.nextInt();
            Decoder decoder = new Decoder(text, key);
            decoder.decode(text, key);
        }
    }
}

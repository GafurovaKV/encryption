public class Encoder {
    public Encoder(String text, int key) {
        this.text = text;
        this.key = key;
    }

    String text;
    int key;

    public static void encode(String text, int key) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char t = text.charAt(i);
            if (t >= 'A' && t <= 'Z') {
                int t1 = t - 'A' + key;
                t1 = t1 % 26;
                sb.append((char) (t1 + 'A'));
            } else if (t >= 'a' && t <= 'z') {
                int t1 = t - 'a' + key;
                t1 = t1 % 26;
                sb.append((char) (t1 + 'a'));
            }
        }
        System.out.println("После шифрования:" + " " + sb);
    }
}



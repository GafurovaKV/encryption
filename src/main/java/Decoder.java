public class Decoder {
    public Decoder(String text, int key) {
        this.text = text;
        this.key = key;
    }

    String text;
    int key;

    public static void decode(String text, int key) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char t = text.charAt(i);
            if (t >= 'A' && t <= 'Z') {
                int t1 = t - 'A' - key;
                if (t1 < 0) t1 = 26 + t1;
                sb.append((char) (t1 + 'A'));
            } else if (t >= 'a' && t <= 'z') {
                int t1 = t - 'a' - key;
                if (t1 < 0) t1 = 26 + t1;
                sb.append((char) (t1 + 'a'));
            }
        }
        System.out.println("После расшифровки:" + " " + sb);
    }
}
